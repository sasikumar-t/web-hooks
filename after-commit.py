from __future__ import absolute_import
import subprocess
import sys
from datetime import datetime

def get_all_commits(count):
    commit_info = []
    commit_result = subprocess.Popen(
                [
                    "git",
                    "log",
                    count
                ], stdout=subprocess.PIPE).stdout.read()
    commit_result = str(commit_result)[2:]
    commit = ""
    for lines in commit_result.split("\\n\\n"):
        if  lines.startswith("commit "):
            for line in lines.split("\\n"):
                if line.startswith("commit"):
                    commit = line[6:].strip()
            commit_info.append(commit)
    return commit_info

def pylint_test(path):

    pylint_result = subprocess.Popen(
                [
                    "pylint",
                    "--rcfile", ".fokal-githooks/fokal_lint.pylintrc",
                    path.strip()
                ], stdout=subprocess.PIPE).stdout.read()
    pylint_result = str(pylint_result)
    if "Your code has been rated at " in pylint_result:
        start = pylint_result.find("Your code has been rated at ")
        start += len("Your code has been rated at ")
        score = pylint_result[start:]
        end = score.find("/")
        score = score[:end]
        print("Score of", path, "is", score)
        return score


def get_changed_files_info(score_file):
    for info in COMMIT_INFO:
        commit = info
        print("*"*10, commit, "*"*10)
        commit_result = subprocess.Popen(
            [
                "git",
                "diff-tree",
                "--no-commit-id",
                "--name-only",
                "-r",
                commit
                ], stdout=subprocess.PIPE).stdout.read()
        commit_result = str(commit_result, 'utf-8')

        for files in commit_result.split("\n"):
            if files.endswith("py"):
                score = pylint_test(files)
                print(files, score)
                score_file.write(files+","+score+"\n")
        print("\n\n")
    score_file.close()


if len(sys.argv) > 1:
    COUNT = str(sys.argv[2])
else:
    COUNT = "-20"
COMMIT_INFO = get_all_commits(COUNT)
date = str(datetime.now())
date = date.replace(" ","_").replace(":","-")
SCORE_FILE = open("result/Pylint-Score-"+date+".log", "w")
get_changed_files_info(SCORE_FILE)
print("The scores are saved in score_info.log")
