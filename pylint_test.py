#git whatchanged --since=LAST_SUCCESSFUL_BUILD --pretty="format:" --name-only HEAD^ | grep '\.py$'
import subprocess,shlex
from datetime import datetime
import os

def get_changed_files():
    command ='''git whatchanged --since=LAST_SUCCESSFUL_BUILD --pretty="format:" --name-only HEAD^ '''
    cmd = shlex.split(command)

    files_since_last_build = subprocess.Popen(cmd    , stdout=subprocess.PIPE).stdout.read()
    if len(files_since_last_build)==0:
        #print("No, files found, since last successful build")
        return []
    files_since_last_build = str(files_since_last_build)[2:-1]
    
    return files_since_last_build.replace("\\n\\n","\\n").split("\\n")

def pylint_test(path,score_file):
    pylint_result = subprocess.Popen(
                [
                    "pylint",
                    "--rcfile", ".fokal-githooks/fokal_lint.pylintrc",
                    path.strip()
                ], stdout=subprocess.PIPE).stdout.read()
    pylint_result = str(pylint_result)
    if "Your code has been rated at " in pylint_result:
        start = pylint_result.find("Your code has been rated at ")
        start += len("Your code has been rated at ")
        score = pylint_result[start:]
        end = score.find("/")
        score = score[:end]
        score_file.write(path+","+score+"\n")
        #print("Score of", path, "is", score)
        return score

try:
    os.mkdir("result")
except:
    pass
date = str(datetime.now())
date = date.replace(" ","_").replace(":","-")
result_path = "result/Pylint-Score-"+date+".log"
SCORE_FILE = open(result_path, "w")

files = get_changed_files()
for file in files:
    if len(file)>0 and file.endswith('py'):
        pylint_test(file,SCORE_FILE)
SCORE_FILE.close()

#print("The result is stored at",SCORE_FILE.name)
print(os. getcwd() + SCORE_FILE.name,end="")
